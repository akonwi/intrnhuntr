class NotepadsController < ApplicationController

  def update
  	@notepad = Notepad.find_by_user_id(current_user.id)
  	@notepad.update_attribute :content, params[:notepad][:content]
  	respond_to do |format|
      format.js
      format.html { redirect_to session[:previous] || root_path }
    end
  end

end
