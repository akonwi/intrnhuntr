class OrdersController < ApplicationController

  before_filter :check_for_user

  def new
    @order = Order.new
  end

  def create
    @order = current_user.build_order(params[:order])
    if @order.save
      if @order.purchase
        flash.now[:success] = "Great! Thanks for your order. Feel free to upload a resume if you have one ready."
        render :success
      else
        flash.now[:error] = "Sorry, there was an error completing the purchase. Please try again and contact us if there are still problems."
        render :new
      end
    else
      render :new
    end
  end

  def success
    @user = current_user
  end

  private

    def check_for_user
      redirect_to root_path unless current_user
    end
end
