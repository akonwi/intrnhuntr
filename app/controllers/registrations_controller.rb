class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super
  end

  def edit
    super
  end

  # overriding this method to send mailer only when user is uploading a resume
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if resource.update_with_password(resource_params)
      if is_navigational_format?
        if update_needs_confirmation?(resource, prev_unconfirmed_email)
          flash_key = :update_needs_confirmation
         else
           flash_key = :updated
           UserMailer.upload_notify(resource).deliver if resource_params[:resume]
        end
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, :bypass => true
      respond_with resource, :location => after_update_path_for(resource)
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def destroy
    super
  end

  def cancel
    super
  end

  def update_needs_confirmation?(resource, previous)
    super
  end

  def build_resource(hash=nil)
    super
  end

  def sign_up(resource_name, resource)
    super
  end

  def after_sign_up_path_for(resource)
    super
  end

  def after_inactive_sign_up_path_for(resource)
    super
  end

  # overriding this to go to previous page or back home
  def after_update_path_for(resource)
    session[:previous] || root_path
  end

  def authenticate_scope!
    super
  end

end