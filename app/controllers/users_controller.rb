class UsersController < ApplicationController
	before_filter :authenticate_user!

## don't need a profile page right now
#  def show
#  	@user = User.find(current_user.id)
#  end

  def favorites
  	@favorites = current_user.favorites params[:page] #favorited_companies.page(params[:page])
  end

end
