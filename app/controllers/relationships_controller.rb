class RelationshipsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @company = Company.find(params[:relationship][:favorited_id])
    current_user.favorite! @company
    respond_to do |format|
      format.html { redirect_to @company }
      format.js
    end
  end

  def destroy
    @company = Relationship.find(params[:id]).favorited
    current_user.unfavorite! @company
    respond_to do |format|
     	format.html { redirect_to @company }
     	format.js
    end
  end
end
