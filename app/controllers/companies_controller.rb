class CompaniesController < ApplicationController

  before_filter :check_for_user, :only => [:show]

  def index
  	flash.now[:info] = "Click any of the top four for sample information" unless user_signed_in?
  	@search = Company.search params[:search]
  	@companies = @search.page params[:page]
  end

  def show
    @company = Company.find(params[:id])
    @positions = Position.where(company_id: @company.id).all
    @requirements = Requirement.where(company_id: @company.id).all
    @questions = Question.where(company_id: @company.id).all
    @rspnbties = Responsibility.where(company_id: @company.id).all
  rescue ActiveRecord::RecordNotFound
    redirect_to companies_path
  end

  private

    def check_for_user
      unless current_user
        if params[:id].to_i > 4
          flash[:error] = "Please login first."
          redirect_to login_path
        end
      end
    end
end
