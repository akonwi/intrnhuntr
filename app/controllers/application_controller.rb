class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :check_for_notepad
  after_filter :store_location

  # make sure the current user has a notepad and create one if not
  def check_for_notepad
  	if user_signed_in?
  		current_user.make_notepad if Notepad.find_by_user_id(current_user.id) == nil
  		notepad
  	end
  end

  #helper method for check_for_notepad
  def notepad
  	@notepad = Notepad.find_by_user_id(current_user.id)
  end

  # store the current location for redirection purposes
  # only store the previous page if if is not a /user or /login path
	def store_location
		session[:previous] = request.fullpath unless request.fullpath =~ /\/users/ || request.fullpath =~ /\/login/
	end

  # overriding devise's method to always go to root after sign_out
	def after_sign_out_path_for(resource_or_scope)
		root_path
	end

  # overriding devise's method to allow Admins to sign in
	def after_sign_in_path_for(resource)
		if resource.instance_of? AdminUser
			'/admin'
		else
			session[:previous] || root_path
		end
	end
end
