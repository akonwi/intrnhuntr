class Position < ActiveRecord::Base
  attr_accessible :company_id, :position
  
  belongs_to :company
end
