class Order < ActiveRecord::Base

  attr_accessible :user_id, :first_name, :last_name, :card_brand, :card_expires_on, :card_number, :card_verification
  attr_accessor :card_number, :card_verification

  belongs_to :user
  has_many :transactions, :class_name => "OrderTransaction"

  validate :validate_card, :on => :create

  def purchase
    response = GATEWAY.purchase 500, credit_card
    transactions.create :action => "purchase", :amount => 500, :response => response
    user.update_attribute(:paid, true) if response.success?
    response.success?
  end

  private

    def validate_card
      unless credit_card.valid?
        credit_card.errors.full_messages.each do |message|
          errors[:base] << message
        end
      end
    end

    def credit_card
      @credit_card ||= ActiveMerchant::Billing::CreditCard.new(
        type: card_brand,
        number: card_number,
        verification_value: card_verification,
        month: card_expires_on.month,
        year: card_expires_on.year,
        first_name: first_name,
        last_name: last_name
      )
    end
end
