class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :notes, :resume, :paid
  attr_accessor :resume_content_type

  has_many :relationships, foreign_key: "favoriter_id", dependent: :destroy
  has_many :favorited_companies, through: :relationships, source: :favorited

  has_one :notepad, dependent: :destroy
  has_one :order

  has_attached_file :resume,
                    :storage => :dropbox,
                    :dropbox_credentials => "#{Rails.root}/config/dropbox.yml",
                    :dropbox_options => {
                      :path => proc { |resume| "resumes/#{id}/#{name}" }
                    }

  validates_attachment :resume, content_type: { content_type: /^application\/(msword|pdf)/ }

  def favorite!(company)
    relationships.create(favorited_id: company.id)
  end

  def favorited?(company)
    relationships.find_by_favorited_id(company.id) ? true : false
  end

  def unfavorite!(company)
    relationships.find_by_favorited_id(company.id).destroy
  end

  def favorites(page)
    favorited_companies.page(page)
  end

  def make_notepad
  	Notepad.create(user_id: self.id, content: "")
  end

  def paid?
    self.paid
  end

end
