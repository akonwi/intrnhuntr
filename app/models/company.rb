class Company < ActiveRecord::Base
  attr_accessible :about, :area, :name, :salary

  has_many :relationships, foreign_key: "favorited_id", dependent: :destroy
  has_many :favoriters, through: :relationships, source: :favoriter

  has_many :positions
  has_many :questions
  has_many :requirements
  has_many :responsibilities

  # meta_search limitations
  attr_searchable :name, :salary, :area
  assoc_unsearchable :relationships, :favoriters

  # active_admin scope
  scope :unfinished, where(:about => "") || where(:area => "") || where(:salary => nil)

  def favorited?(user)
    relationships.find_by_favoriter_id(user.id) ? true : false
  end

end
