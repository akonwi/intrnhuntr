class Responsibility < ActiveRecord::Base
  attr_accessible :company_id, :responsibility
  
  belongs_to :company
end
