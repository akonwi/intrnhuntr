class Requirement < ActiveRecord::Base
  attr_accessible :company_id, :requirement
  
  belongs_to :company
end
