class Question < ActiveRecord::Base
  attr_accessible :company_id, :question
  
  belongs_to :company
end
