class Relationship < ActiveRecord::Base
  attr_accessible :favorited_id, :favoriter_id
  
  belongs_to :favoriter, class_name: "User"
  belongs_to :favorited, class_name: "Company"

  validates :favoriter_id, presence: true
  validates :favorited_id, presence: true
end
