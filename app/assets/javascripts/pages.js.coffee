# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->

	title = $(document).attr('title')
	if title == "intrnhuntr"
		$('#home').addClass('active')
	else if title == "intrnhuntr | Services"
		$('#services').addClass('active')
	else if title == "intrnhuntr | Companies"
		$('#internships').addClass('active')


jQuery ->
  # this variable will store the current content of the notepad
  # and put back in if the notepad is closed without being saved
	notes = $('#notepad_content').val()

	$('#show_notes').on 'click', (event) ->
		$('#my_modal').modal({keyboard: true})
		$('#my_modal').draggable({handle: '.modal-header'})
		event.preventDefault()

	$('#close_modal').on 'click', (event) ->
		$('#my_modal').modal('hide')
		$('#notepad_content').val(notes)
		event.preventDefault()

	$('#save').on 'click', (event) ->
		notes = $('#notepad_content').val()
