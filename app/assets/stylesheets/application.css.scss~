/* use sass @imports here */

// First import variables
@import "bootswatch/cyborg/variables";

// Then bootstrap itself AND ALL BOOTSTRAP VARIABLE CHANGES
$inputBackground: $grayDark;
@import "bootstrap";
@import 'font-awesome';

// Bootstrap body padding for fixed navbar
body { padding-top: 60px; }

// Responsive styles go here in case you want them
@import "bootstrap-responsive";

// And finally bootswatch style itself
@import "bootswatch/cyborg/bootswatch";

// Whatever application styles you have go last

@mixin box_sizing {
  -moz-box-sizing: border-box; 
  -webkit-box-sizing: border-box; 
  box-sizing: border-box;
}

.white-caret {
  border-top-color: $white !important;
  border-bottom-color: $white !important;
}

.favorite {
  border: none;
  height: 32px;
  width: 32px;
}

html {
  overflow-y: scroll;
}

section {
  overflow: auto;
}

textarea {
  resize: horizontal vertical;
  background: $white;
}

.right {
  text-align: right;
}

.center {
  text-align: center;
  display: block;
  margin-left: auto;
  margin-right: auto;
  align: center;
  
  h1 {
    margin-bottom: 10px;
  }
}

.nav-search {
  padding: 4px 8px 0px 0px;
  height: 25px;
}

/* typography */

h1, h2, h3, h4, h5, h6 {
  line-height: 1;
}

h1 {
  font-size: 3em;
  letter-spacing: -2px;
  margin-bottom: 30px;
  /*text-align: center;*/
}

h2 {
  font-size: 1.7em;
  letter-spacing: -1px;
  margin-bottom: 30px;
  text-align: right;
  font-weight: normal;
  color: #999;
}

p {
  font-size: 1.1em;
  line-height: 1.7em;
}


/* header */

#logo {
  float: left;
  margin-right: 10px;
  font-size: 1.7em;
  //color: #fff;
  color: $white;
  text-transform: lowercase;
  text-decoration: none;
  letter-spacing: -1px;
  padding-top: 9px;
  font-weight: bold;
  line-height: 1;
}

#logo:hover {
  //color: #fff;
  color: $white;
  text-decoration: none;
}

/* footer */

footer {
  margin-top: 45px;
  padding-top: 5px;
  border-top: 1px solid #eaeaea;
  color: #999;
}

footer a {
  color: #555;
}  

footer a:hover { 
  color: #222;
}

footer small {
  float: left;
}

footer ul {
  float: right;
  list-style: none;
}

footer ul li {
  float: left;
  margin-left: 10px;
}

/* miscellaneous */

.debug_dump {
  clear: both;
  float: left;
  width: 100%;
  margin-top: 45px;
  @include box_sizing;
}

/* sidebar */

aside {
  section {
    padding: 10px 0;
    &:first-child {
      border: 0;
      padding-top: 0;
    }
    span {
      display: block;
      margin-bottom: 3px;
      line-height: 1;
    }
    h1 {
      font-size: 1.6em;
      text-align: left;
      letter-spacing: -1px;
      margin-bottom: 3px;
    }
  }
}

/* forms */

input, textarea, select, .uneditable-input {
  border: 1px solid #bbb;
//  width: 100%;
  padding: 10px;
  height: auto;
  margin-bottom: 15px;
  input-color: black;
  //@include box_sizing;
}

#error_explanation {
  color: #f00;
  ul {
    list-style: none;
    margin: 0 0 18px 0;
  }
}

.field_with_errors {
  @extend .control-group;
  @extend .error;
 }

// companies index 
.styled-select select, option {
  background: $grayDark;
  color: $grayLight;
  width: 200px;
  padding: 0px;
  //font-size: 15px;
  border: 1px solid #ccc;
  height: 27px;
  //@include box_sizing;
  overflow: hidden;
}

.field {
	padding: 2px;
	padding-bottom: 5px;
}

// side bar
.sidebar-nav {
  padding: 5px 5px 5px 5px;
  position: fixed;
}

#save {
  position: fixed;
}

#notes {
  padding-right: 12px;
}
.notepad {
  textarea {
    resize: vertical;
    width: 140px;
    height: 200px;
  }
}



