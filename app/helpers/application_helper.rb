module ApplicationHelper

	# return full title on per page basis
  def full_title(page_title)
    if page_title.empty?
      "intrnhuntr"
    else
      "intrnhuntr | #{page_title}"
    end
  end

end
