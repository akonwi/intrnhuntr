module DeviseHelper

  # overriding DeviseHelper
	def devise_error_messages!
		return "" if resource.errors.empty?
    # show all errors in one sentence. I forgot what it shows by default
		messages = resource.errors.full_messages
		flash.now[:error] = messages.join " and "
	end

end
