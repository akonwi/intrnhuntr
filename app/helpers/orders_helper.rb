module OrdersHelper

  def order_error_messages!(order)
    return "" if order.errors.empty?
    # only show the first error message. I forgot what it shows by default
    messages = order.errors.full_messages
    flash.now[:error] = messages.first
  end

end
