ActiveAdmin.register Position do
  
  menu parent: "Companies"
  filter :company
  
  index download_links: false do
		column :position
		column :company, sortable: :company
		default_actions
  end
end
