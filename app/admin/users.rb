ActiveAdmin.register User do

  index do
    column :name, sortable: :name
    column :id, sortable: :id
    default_actions
  end

  form html: { enctype: "multipart/form-data" } do |f|
    f.inputs "Details" do
      f.input :resume, label: "Revised Resume", as: :file
    end
    f.buttons
  end
end
