ActiveAdmin.register Responsibility do
	
	menu parent: "Companies"
  filter :company
  
  index download_links: false do
		column :responsibility
		column :company, sortable: :company
		default_actions
  end
end
