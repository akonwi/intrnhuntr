ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }
  
  content title: 'Dashboard' do
  	columns do
  		column do
				panel 'Recently Added Companies' do
					table_for Company.order('id desc').limit 10 do
						column :name do |company|
							link_to company.name, [:admin, company]
						end
						column :area 
					end 
				strong { link_to 'View all', admin_companies_path }
				end
  		end
  	end
  end
    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
end
