ActiveAdmin.register Requirement do

	menu parent: "Companies"
  filter :company
  
  index download_links: false do
		column :requirement
		column :company, sortable: :company
		default_actions
  end
end
