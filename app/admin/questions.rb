ActiveAdmin.register Question do

	menu parent: "Companies"
  filter :company
  
  index download_links: false do
		column :question
		column :company, sortable: :company
		default_actions
  end
end
