class UserMailer < ActionMailer::Base
  default from: "intrns@intrnhuntr.com"

  # when a resume gets uploaded, notify the intrnhuntrs
  def upload_notify(user)
    @user = user
    mail(to: "intrnhuntr@gmail.com", from: user.email, subject: "New Resume")
  end
end
