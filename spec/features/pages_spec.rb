require 'spec_helper'

describe "Pages are correct" do

  describe "home page" do
    it "says 'welcome'" do
      visit root_path
      page.should have_content 'Welcome to intrnhuntr'
    end
  end

  describe "internships page" do
    it "says 'Internships'" do
      visit companies_path
      page.should have_content 'Internships'
    end
  end
end
