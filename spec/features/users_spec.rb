require 'spec_helper'

describe "users" do

  describe "user signs up with valid info" do
    it "will show confirmation" do
      visit signup_path
      fill_in 'Name', with: 'thomas'
      fill_in 'Email', with: 'foobar@gmail.com'
      first(:field, 'Password').set 'foobar'
      fill_in 'Password confirmation', with: 'foobar'
      click_button 'Sign up'
      page.should have_content 'A message with a confirmation link has been sent to your email address. Please open the link to activate your account.'
    end
  end

  describe "user signs up with INvalid info" do
    it "will show errors" do
      visit signup_path
      click_button 'Sign up'
      page.should have_css 'div.alert'
    end
  end

  describe "user confirms account" do
    it "will sign them in" do
      user = FactoryGirl.create :user
      user.confirm!
      visit login_path
      fill_in 'Email', with: 'foobar@gmail.com'
      fill_in 'Password', with: 'foobar'
      click_button 'Login'
      page.should have_content 'Logged'
    end
  end

  describe "user has not paid for resume stuff" do
    it "will say 'GET RESUME CRITIQUE'" do
      user = FactoryGirl.create :user
      user.confirm!

      visit login_path
      fill_in 'Email', with: 'foobar@gmail.com'
      fill_in 'Password', with: 'foobar'
      click_button 'Login'
      page.should have_content 'Logged in successfully.'

      visit favorites_path
      page.should have_content 'Resume Critique'
    end
  end

  describe "user has paid for resume stuff" do
    it "will say 'upload it here'" do
      user = FactoryGirl.create :user
      user.confirm!
      user.update_attribute :paid, true

      visit login_path
      fill_in 'Email', with: 'foobar@gmail.com'
      fill_in 'Password', with: 'foobar'
      click_button 'Login'
      page.should have_content 'Logged in successfully.'

      visit favorites_path
      page.should have_content "resume"
    end
  end
end
