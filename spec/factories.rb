FactoryGirl.define do
  factory :user do
    name 'thomas'
    email 'foobar@gmail.com'
    password 'foobar'
    password_confirmation 'foobar'
    resume Rack::Test::UploadedFile.new(Rails.root + 'spec/files/Akonwi.pdf', 'application/pdf')
  end

  factory :resume do
    resume_file_name 'thomas.pdf'
    resume_content_type 'application/pdf'
    resume_file_size '1096'
  end
end