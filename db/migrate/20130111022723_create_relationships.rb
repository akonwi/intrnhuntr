class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :favoriter_id
      t.integer :favorited_id

      t.timestamps
    end
  
		add_index :relationships, :favoriter_id
		add_index :relationships, :favorited_id
		add_index :relationships, [:favorited_id, :favoriter_id], unique: true
  end
end
