class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :area
      t.integer :salary
      t.text :about

      t.timestamps
    end
  end
end
