class CreateNotepads < ActiveRecord::Migration
  def change
    create_table :notepads do |t|
      t.integer :user_id
      t.text :content

      t.timestamps
    end
  end
end
