class AddPaidToUsers < ActiveRecord::Migration
  def change
    add_column :users, :paid, :boolean
    User.all.each do |user|
      user.paid = false
    end
  end
end
