class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.integer :company_id
      t.text :position

      t.timestamps
    end
  end
end
