class CreateRequirements < ActiveRecord::Migration
  def change
    create_table :requirements do |t|
      t.integer :company_id
      t.text :requirement

      t.timestamps
    end
  end
end
