class CreateResponsibilities < ActiveRecord::Migration
  def change
    create_table :responsibilities do |t|
      t.integer :company_id
      t.text :responsibility

      t.timestamps
    end
  end
end
