Intrnhuntr::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  devise_for :users, :controller => { :registrations => 'registrations' }

  as :user do
  	# get '/profile' => 'users#show'
  	get '/login' => 'devise/sessions#new'
  	delete '/logout' => 'devise/sessions#destroy'
  	get '/favorites' => 'users#favorites'
  	get '/signup' => 'registrations#new'
  end

	resources :companies, :only => [:show, :index]
	resources :notepads, :only => :update
  resources :relationships, :only => [:create, :destroy]
  resources :orders

  root :to => 'pages#home'

  match '/services', :to => 'pages#services'
  match '*path', :to => 'pages#home'

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

end
